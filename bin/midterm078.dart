import 'dart:math';

import 'package:midterm078/midterm078.dart' as midterm078;
import 'dart:io';

tokenizing(var expression) {
  //midterm exercise 1
  List<String> tokenList = [];
  tokenList = expression.split(' ');

  return expression = tokenList;
}

infixPostfix(var tokenOut) {
  //midterm exercise 2
  int pcd = 0; // operator precedence
  int pcdLast = 0;

  List<String> listPostfix = [];
  List<String> listOperator = [];

  for (int i = 0; i < tokenOut.length; i++) {
    //if token is integer
    if (tokenOut[i] == "0" ||
        tokenOut[i] == "1" ||
        tokenOut[i] == "2" ||
        tokenOut[i] == "3" ||
        tokenOut[i] == "4" ||
        tokenOut[i] == "5" ||
        tokenOut[i] == "6" ||
        tokenOut[i] == "7" ||
        tokenOut[i] == "8" ||
        tokenOut[i] == "9") {
      listPostfix.add(tokenOut[i]);
    }

    //if token is operator
    if (tokenOut[i] == "+" ||
        tokenOut[i] == "-" ||
        tokenOut[i] == "*" ||
        tokenOut[i] == "/" ||
        tokenOut[i] == "%" ||
        tokenOut[i] == "^") {
      if (tokenOut[i] == "(" || tokenOut[i] == ")") {
        // () = 0
        pcd = 0;
      } else if (tokenOut[i] == "+" || tokenOut[i] == "-") {
        // + - = 1
        pcd = 1;
      } else if (tokenOut[i] == "*" || tokenOut[i] == "/") {
        // * / = 2
        pcd = 2;
      } else {
        // % ^ = 3
        pcd = 3;
      }

      if (listOperator.isNotEmpty) {
        switch (listOperator.last) {
          case "+":
            pcdLast = 1;
            break;
          case "-":
            pcdLast = 1;
            break;
          case "*":
            pcdLast = 2;
            break;
          case "/":
            pcdLast = 2;
            break;
          case "^":
            pcdLast = 3;
            break;
          case "%":
            pcdLast = 3;
            break;

          default:
        }
      }

      //while operator not empty and last in listOperator not open parenthesis and pcd <= lastpcd then remove and add to listPostfix
      while (listOperator.isNotEmpty &&
          listOperator.last != "(" &&
          pcd <= pcdLast) {
        listPostfix.add(listOperator.removeLast());
      }

      listOperator.add(tokenOut[i]);
    }

    //if open parenthesis
    if (tokenOut[i] == "(") {
      listOperator.add(tokenOut[i]);
    }

    //if close parenthesis
    if (tokenOut[i] == ")") {
      while (listOperator.last != "(") {
        listPostfix.add(listOperator.removeLast());
      }
      listOperator.remove("(");
    }
  }

  //while listOperator not empty then get last item from listOperator and add to listPostfix
  while (listOperator.isNotEmpty) {
    listPostfix.add(listOperator.removeLast());
  }

  return listPostfix;
}

//midterm exercise 3

evaluate(var listPostfix) {
  List<double> values = [];

  double sum;

  for (int i = 0; i < listPostfix.length; i++) {
    if (listPostfix[i] == "0" ||
        listPostfix[i] == "1" ||
        listPostfix[i] == "2" ||
        listPostfix[i] == "3" ||
        listPostfix[i] == "4" ||
        listPostfix[i] == "5" ||
        listPostfix[i] == "6" ||
        listPostfix[i] == "7" ||
        listPostfix[i] == "8" ||
        listPostfix[i] == "9") {
      double number = double.parse(listPostfix[i]);
      values.add(number);
    } else {
      double right;
      double left;

      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      sum = 0;

      if (listPostfix[i] == "+") {
        sum = left + right;
      }
      if (listPostfix[i] == "-") {
        sum = left - right;
      }
      if (listPostfix[i] == "*") {
        sum = left * right;
      }
      if (listPostfix[i] == "/") {
        sum = left / right;
      }
      if (listPostfix[i] == "^") {
        sum = 0.0 + pow(left, right);
      }
      if (listPostfix[i] == "%") {
        sum = left % right;
      }

      values.add(sum);
    }
  }
  return values[0];
}

void main(List<String> arguments) {
  print('Input mathematical expression: ');
  String? expression = stdin.readLineSync()!;
  List<String> tokenOut = tokenizing(expression);
  print(tokenOut); //midterm output exercise 1
  List<String> postfixOut = infixPostfix(tokenOut);
  print(postfixOut); //midterm output exercise 2
  double ans = evaluate(postfixOut);
  print(ans); //midterm output exercise 3
}
